import { Injectable } from '@angular/core';


// Importaciones de Firebase / Firestore
import { Firestore } from '@angular/fire/firestore';
import { AngularFirestore, AngularFirestoreCollection, DocumentReference } from '@angular/fire/compat/firestore';
import { IContact } from '../models/contact.interface';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class FirebaseService {

  private _contactosCollection: AngularFirestoreCollection<IContact>;
  private _contactos$: Observable<IContact[]>

  constructor(private _db: AngularFirestore) {

    this._contactosCollection = this._db.collection<IContact>('contactos');

    // Binding automatizado con los cambios en la colección de contactos de firestore
    this._contactos$ = this._contactosCollection.snapshotChanges().pipe(
      map(actions => {
        return actions.map(action => {
          const data = action.payload.doc.data(); // los datos de documento, sin el ID
          const id = action.payload.doc.id; // ID del documento
          return { id, ...data }
        })
      })
    );
  }


  getAllContacts(): Observable<IContact[]>{
    return this._contactos$;
  }

  getContactById(id: string): Observable<IContact | undefined>{
    return this._contactosCollection.doc<IContact>(id).valueChanges();
  }

  createContact(contact: IContact): Promise<DocumentReference<IContact>>{
    return this._contactosCollection.add(contact);
  }

  updateContactById(id: string, contact: IContact): Promise<void>{
    return this._contactosCollection.doc<IContact>(id).update(contact);
  }

  deleteContactById(id: string){
    return this._contactosCollection.doc<IContact>(id).delete();
  }

}
