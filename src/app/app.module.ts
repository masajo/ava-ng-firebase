import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

/**
 * * Módulos de Firebase y FireStore
 */
import { AngularFireModule } from '@angular/fire/compat';
import { FirestoreModule, getFirestore, provideFirestore } from '@angular/fire/firestore';
import { provideFirebaseApp, initializeApp } from '@angular/fire/app';


import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

// Variables de entorno
import { environment } from '../environments/environment';
import { HomePageComponent } from './pages/home-page/home-page.component'

@NgModule({
  declarations: [
    AppComponent,
    HomePageComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    // Módulos para trabajar con firebase
    AngularFireModule.initializeApp(environment.firebase),
    FirestoreModule,
    // Opción II
    provideFirebaseApp(() => initializeApp(environment.firebase)),
    provideFirestore(() => getFirestore()),
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
