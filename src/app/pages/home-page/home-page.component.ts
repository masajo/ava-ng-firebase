import { Component, OnInit } from '@angular/core';
import { IContact } from 'src/app/models/contact.interface';
import { FirebaseService } from 'src/app/services/firebase.service';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.css']
})
export class HomePageComponent implements OnInit {

  listaContactos: IContact[] = [];

  constructor(private _firebaseService: FirebaseService) { }

  ngOnInit(): void {

    this._firebaseService.getAllContacts().subscribe(
      (respuesta: IContact[]) => {
        console.table(respuesta);
        this.listaContactos = respuesta
      },
      (error) => alert(`Error al obtener los contactos de Firebase: ${error}`),
      () => console.log('Contactos obtenidos con éxito')
    )
  }

}
