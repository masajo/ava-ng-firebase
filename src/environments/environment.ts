// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyDpvcZTZmlHMy0dY4rP27F5ap9UwxaI_hg',
    authDomain: 'ava-ng.firebaseapp.com',
    projectId: 'ava-ng',
    storageBucket: 'ava-ng.appspot.com',
    messagingSenderId: '183324818161',
    appId: '1:183324818161:web:5fa48d51319717317c4750',
    measurementId: 'G-PWBTMLXQX1',
  },
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
